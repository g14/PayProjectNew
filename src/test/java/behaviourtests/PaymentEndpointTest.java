package behaviourtests;

import com.google.inject.Inject;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import rest.PaymentEndpoint;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.restassured.RestAssured.given;

/**
 * 
 * @author Kevin Fleming
 *
 */
@QuarkusTest
@TestHTTPEndpoint(PaymentEndpoint.class)
public class PaymentEndpointTest {


    @Inject
    PaymentEndpoint paymentEndpoint;


        @BeforeAll
        public static void setup() {
            PaymentEndpoint mock = Mockito.mock(PaymentEndpoint.class);
            Mockito.when(mock.receiveData("token","token",0,"test")).thenReturn(true);
            QuarkusMock.installMockForType(mock, PaymentEndpoint.class);
        }

    @Test
    public void testRegisterCustomer(){

        given().contentType(ContentType.JSON)
                .param("token", "token")
                .param("merchantid", "0")
                .param("amount", 0)
                .param("description", "test")
                .when().get()
                .then().statusCode(200);
    }


}
