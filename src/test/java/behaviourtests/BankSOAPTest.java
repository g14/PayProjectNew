package behaviourtests;

import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import payment.BasicPayment;

/** THE TESTS USE CPR NUMBERS
 * 
 * @author Leonardo Zecchin
 *
 */

public class BankSOAPTest {
	BasicPayment bp;
	private static BankService bankService;
	User testUser, merchantUser;
	String testUserString, merchantUserString;
	boolean result;
	BigDecimal amount, previousBalanceC,previousBalanceM;
	
	@Before
	public void setUp() {
		BankServiceService service = new BankServiceService();
		bankService = service.getBankServicePort();
		bp = new BasicPayment();
	}
	
	@Given ("two accounts")
	public void two_accounts() {
		try {
			bankService.getAccountByCprNumber("111111-1111").getId();
		} catch (BankServiceException_Exception e1) {
			testUser = new User();
			testUser.setFirstName("Test");
			testUser.setLastName("User");
			testUser.setCprNumber("111111-1111");
			try {
				bankService.createAccountWithBalance(testUser, new BigDecimal(10000));
			} catch (BankServiceException_Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		try {
			bankService.getAccountByCprNumber("222222-2222").getId();
			
		} catch (BankServiceException_Exception e1) {
			merchantUser = new User();
			merchantUser.setFirstName("Test");
			merchantUser.setLastName("Merchant");
			merchantUser.setCprNumber("222222-2222");
			amount= new BigDecimal(50);
			try {
				bankService.createAccountWithBalance(merchantUser, new BigDecimal(20000));
			} catch (BankServiceException_Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	@When ("the customer {string} sends the merchant {string} money {int}")
	public void theCustomerSendsTheMerchantMoney(String customerCpr, String merchantCpr, int amount) {
		try {
			testUserString = bankService.getAccountByCprNumber(customerCpr).getId();
			merchantUserString=bankService.getAccountByCprNumber(merchantCpr).getId();
			previousBalanceM=bankService.getAccount(merchantUserString).getBalance();
			previousBalanceC=bankService.getAccount(testUserString).getBalance();
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
		BigDecimal bdAmount= new BigDecimal(amount);
		System.out.println(testUserString+ " " + merchantUserString +" " + bdAmount);
		result =bp.contactBank(testUserString, merchantUserString, bdAmount.intValue(),"test");
		System.out.println(result);
	}
	
	@Then ("the operation is succesful")
	public void the_operation_is_succesful() {
		assertTrue(result);
	}
	
	@Then("the operation is not successful")
	public void theOperationIsNotSuccesful() {
		assertFalse(result);
	}
}
