package behaviourtests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.CustomerIdReceiver;
import messaging.CustomerIdSender;
import messaging.Event;
import messaging.RabbitMqReceiver;
import messaging.RabbitMqSender;
import messaging.TokenReceiver;
import messaging.TokenSender;
import payment.*;

import static org.junit.Assert.assertTrue;

/**
 * @author Leonardo Zecchin
 */
public class BasicPaymentTest {

    private String token,merchant,cid;
    private Boolean successfulToken, succesfulCid;
    private TokenReceiver tokenReceiver;
    private TokenSender tokenSender;
    private CustomerIdSender cidSender;
    private CustomerIdReceiver cidReceiver;
    private int amount;
    private Event testEvent;

    public BasicPaymentTest() {
    	successfulToken=false;
    	succesfulCid = false;
		tokenSender = new TokenSender(new RabbitMqSender() {
			
			@Override
			public void send(Event event) throws Exception {
				System.out.println(event + " sent");
			}
		});
		tokenReceiver = new TokenReceiver(new RabbitMqReceiver() {
			
			@Override
			public void receive(Event event) throws Exception {
				System.out.println(event + " received");
				successfulToken=true;
		}});
		cidSender = new CustomerIdSender(new RabbitMqSender() {
			
			@Override
			public void send(Event event) throws Exception {
				System.out.println(event + " sent");
			}
		});
		cidReceiver = new CustomerIdReceiver(new RabbitMqReceiver() {
			
			@Override
			public void receive(Event event) throws Exception {
				System.out.println(event + " received");
				succesfulCid=true;
			}
		});
		testEvent=new Event("Test");
	}
    
    @Given ("a customer with token {string}")
    public void aCustomerWithToken(String token){
        this.token = token;
    }
    @Given("a merchant with name {string}")
    public void aMerchantWithName(String merchant) {
        this.merchant = merchant;
    }
    @Given("payment of amount {int}")
    public void paymentOfAmount(int amount) throws Exception {
        this.amount=amount;
    }
    
    @When("token is sent")
    public void tokenIsSent() throws Exception {
        tokenSender.doSender(testEvent);
    }
    
    @Then("receive customer id")
    public void receiveCustomerId() {
    	tokenReceiver.doReceive(testEvent);
    }
    
    @Then("payment is successful")
    public void paymentIsSuccessful() {
        assertTrue(successfulToken);
    }
    
    @Given("the customer id {string} is received from a valid token")
    public void theAccountIdIsReceivedFromAValidToken(String cid) {
    	this.cid = cid;
    }
    
    @When("the customer id is sent to the account service")
    public void theCustomerIdIsSentToTheAccountService() {
    	cidSender.doSender(testEvent);
    }
    
    @Then("the bank account is received")
    public void theBankAccountIsReceived() {
    	cidReceiver.doReceive(testEvent);
    	assertTrue(succesfulCid);
    }
}
