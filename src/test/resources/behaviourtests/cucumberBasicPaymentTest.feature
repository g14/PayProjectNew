Feature: BasicPaymentTest

  Scenario: token exchange
    Given a customer with token "0"
    Given a merchant with name "x"
    Given payment of amount 0
    When token is sent
    Then receive customer id
    Then payment is successful
    
	Scenario: customer id exchange
		Given the customer id "0" is received from a valid token
		When the customer id is sent to the account service
		Then the bank account is received
		
		