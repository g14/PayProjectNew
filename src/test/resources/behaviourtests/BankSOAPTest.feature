Feature: Bank SOAP Tests

  Scenario: Send money to merchant
    Given two accounts
    When the customer "111111-1111" sends the merchant "222222-2222" money 0
    Then the operation is succesful

	Scenario: The customer does not have enough money
		Given two accounts
		When the customer "111111-1111" sends the merchant "222222-2222" money 999999
		Then the operation is not successful