package payment;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import messaging.*;

import messaging.TransactionSender;
/**
 * The whole payment microservice was written by Leonardo Zecchin and Kevin Fleming
 * main contributor: Leonardo Zecchin
 */
public class BasicPayment implements Payment {
	private BankService service;
	private TokenReceiver tokenReceiver;
	private TokenSender tokenSender;
	String customerAccount,merchantAccount ;
	String[] response,accounts;
	private boolean success;
	private CompletableFuture<Boolean> result, resultCid;
	private final String dateFormat = "yyyy-MM-dd HH:mm:ss";
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
	private Integer[] answer = new Integer[2];
	
	public BasicPayment() {
		service = new BankServiceService().getBankServicePort();
		success = false;
		customerAccount = "";
		response = new String[2];
		accounts = new String[2];
	}

	@Override
	public boolean pay(String token, String merchant, int amount, String description) throws Exception {
		System.out.println("PAY REQUEST " + amount);
		String[] accountReceived = getBankAccountFromToken(token,merchant);
		if (accountReceived[0] != null&&accountReceived[1] != null){
			if(contactBank(accountReceived[0], accountReceived[1], amount,description)) {
				Event transaction = new Event("AddTransactionQueue", new Object[] {answer[1],answer[0],amount,token,LocalDateTime.now().format(formatter),description});
				TransactionSender transactionSender = new TransactionSender();
				transactionSender.send(transaction);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Internal method that, once the token is received via REST from the client, forwards the token to the token management microservice and validates it
	 * @param token
	 * @return if the token is valid it returns the customer's account id, otherwise it returns an empty string
	 * @throws Exception
	 */
	private String[] getBankAccountFromToken(String token,String merchant) throws Exception {
		result = new CompletableFuture<Boolean>();
		tokenReceiver = new TokenReceiver(new RabbitMqReceiver() {
			@Override
			public void receive(Event event) throws Exception {
				if (checkTokenValid(event)) {
					System.out.println("received " + event.getArguments()[1]);
					Object[] arguments = event.getArguments();
					
					//the 3rd position of the valid response contains the token so that if there are many calls then it can be distinguished
					if (((String) arguments[2]).equals(token)) {
						Double d = (Double) arguments[1];
						response = getAccountFromId(d.intValue(),Integer.parseInt(merchant));
					}
				} else {
					System.out.println("not succeeded");
					success = false;
				}
				result.complete(success);
			}
		});
		tokenReceiver.listen();
		tokenSender = new TokenSender();
		Event e = new Event("TokenToBeValidatedQueue", new Object[] { token });
		tokenSender.send(e);
		result.join();
		System.out.println("success " + success);
		if (success) {
			return response;
		} else {
			return new String[2];
		}
	}

	/**
	 * If the token is valid it contains true in the first position of the response arguments
	 * @param event 
	 * @return whether the token was valid or not
	 */
	private Boolean checkTokenValid(Event event) {
		return (Boolean) event.getArguments()[0];
	}

	/**
	 * Forwards the customer id from token management to account management to get the customer's account
	 * @param customerId
	 * @return the account
	 * @throws Exception
	 */
	private String[] getAccountFromId(int customerId, int merchantId) throws Exception {
		resultCid = new CompletableFuture<Boolean>();
		answer[0]=customerId;
		answer[1]=merchantId;
		CustomerIdReceiver cidReceiver = new CustomerIdReceiver(new RabbitMqReceiver() {
			@Override
			public void receive(Event event) throws Exception {
				System.out.println("accounts " + event.getArguments());
				customerAccount = (String) event.getArguments()[0];
				merchantAccount = (String) event.getArguments()[1];
				accounts[0] = customerAccount;
				accounts[1] = merchantAccount;
				success = true;
				resultCid.complete(true);
			}
		});
		cidReceiver.listen();
		CustomerIdSender cidSender = new CustomerIdSender();
		Event e = new Event("CustomerIdQueue", new Object[] { customerId,merchantId });
		cidSender.send(e);

		resultCid.join();
		return accounts;
	}

	/**
	 * Contacts the bank via soap
	 * @param customerAccount received by the account microservice
	 * @param merchant
	 * @param amount
	 * @param description
	 * @return whether the transaction was successful or not
	 */
	public Boolean contactBank(String customerAccount, String merchant, int amount, String description)  {
		try {
			System.out.println("BALANCE "+ service.getAccount(customerAccount) + " FOR AMOUNT " + amount);
			service.transferMoneyFromTo(customerAccount, merchant, new BigDecimal(amount), description);
		} catch (BankServiceException_Exception e) {
			//e.printStackTrace();
			return false;
		}
		return true;
	}

}
