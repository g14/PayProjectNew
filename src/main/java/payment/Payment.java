package payment;
public interface Payment {

    boolean pay(String token, String merchant, int amount, String description) throws Exception;
}
