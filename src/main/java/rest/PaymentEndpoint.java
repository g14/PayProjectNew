package rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import payment.BasicPayment;

/**
 * @author Leonardo Zecchin
 */
@Path("/payment")
@RequestScoped
public class PaymentEndpoint {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public boolean receiveData(@QueryParam("token") String token, @QueryParam("merchantid") String mid, @QueryParam("amount") int amount, @QueryParam("description") String description) {
		BasicPayment payment = new BasicPayment();
		try {
			return payment.pay(token, mid, amount,description);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
