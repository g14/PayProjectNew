package messaging;

public interface RabbitMqSender {
	public void send(Event event) throws Exception;
}
