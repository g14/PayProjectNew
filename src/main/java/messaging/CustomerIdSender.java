package messaging;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author Leonardo Zecchin
 */
public class CustomerIdSender implements RabbitMqSender {
	private static final String EXCHANGE_NAME = "eventsExchange";
	private static final String QUEUE_TYPE = "topic";
	private static final String TOPIC = "CustomerIdQueue";
	ConnectionFactory factory;
    Channel channel;
	private RabbitMqSender rs;
	
	public CustomerIdSender(RabbitMqSender rs) {
		this.rs=rs;
	}
	
	public CustomerIdSender() {
	}
	
	public void doSender(Event event) {
		if (rs != null) {
			try {
				rs.send(event);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void send(Event event) throws Exception {
		factory = new ConnectionFactory();
        factory.setHost("broker");
        try (Connection connection = factory.newConnection();Channel channel = connection.createChannel();){
            channel.exchangeDeclare(EXCHANGE_NAME, QUEUE_TYPE);
            String message = new Gson().toJson(event);
            channel.basicPublish(EXCHANGE_NAME, TOPIC, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
	}

}
