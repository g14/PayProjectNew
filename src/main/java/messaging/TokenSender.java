package messaging;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * I know the rabbitmq classes could be written without repetition but we didn't have time
 * @author Kevin Fleming
 */
public class TokenSender implements RabbitMqSender{
	private static final String EXCHANGE_NAME = "eventsExchange";
	private static final String QUEUE_TYPE = "topic";
	private static final String TOPIC = "TokenToBeValidatedQueue";
    ConnectionFactory factory;
    Channel channel;
    String queueName = "TokenToBeValidatedQueue";
    RabbitMqSender rq;
    
    public TokenSender(RabbitMqSender rq) {
    	this.rq=rq;
    }
    
    public TokenSender() {}
    
    public void doSender(Event event) {
    	if(rq!=null) {
	    	try {
				rq.send(event);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }

    public void send(Event event) throws Exception {
        factory = new ConnectionFactory();
        factory.setHost("broker");
        try (Connection connection = factory.newConnection();Channel channel = connection.createChannel();){
            channel.exchangeDeclare(EXCHANGE_NAME, QUEUE_TYPE);
            String message = new Gson().toJson(event);
            channel.basicPublish(EXCHANGE_NAME, TOPIC, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }


}
