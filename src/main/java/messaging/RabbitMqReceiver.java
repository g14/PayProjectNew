package messaging;
public interface RabbitMqReceiver {
	public void receive(Event event) throws Exception;
}
