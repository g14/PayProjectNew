package messaging;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * @author Leonardo Zecchin
 */
public class TokenReceiver {

    private static final String EXCHANGE_NAME = "eventsExchange";
    private static final String QUEUE_TYPE = "topic";
    private static final String TOPIC = "TokenValidatedQueue";
    RabbitMqReceiver rq;
    
    public TokenReceiver(RabbitMqReceiver rq) {
    	this.rq=rq;
    }
    
    public void doReceive(Event event) {
    	try {
			rq.receive(event);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void listen() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        //factory.setUsername("rabbitmq");
        //factory.setPassword("rabbitmq");
        factory.setHost("broker");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME,QUEUE_TYPE);

        //channel.queueDeclare(channelName, false, false, false, null);
        String queueName= channel.queueDeclare().getQueue();
        channel.queueBind(queueName,EXCHANGE_NAME, TOPIC);
        System.out.println("running token receiver");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        	
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            Event event = new Gson().fromJson(message, Event.class);
            
            try {
            	rq.receive(event);
            }catch (Exception e) {
                    e.printStackTrace();
            }
            
        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });

    }
}
